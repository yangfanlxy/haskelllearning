module Test9 where

import  Data.Char
type MyParser a = String -> [(a, String)]

myReturn :: a -> MyParser a
myReturn v = \inp -> [(v, inp)]

failure :: MyParser a
failure = \inp -> []

item :: MyParser Char
item = \inp	->case inp of
					[] -> []
					(x : xs) -> [(x, xs)]

parse :: MyParser a -> String -> [(a, String)]					
parse p inp = p inp

(+++) :: MyParser a -> MyParser a -> MyParser a
p +++ q = \inp -> case parse p inp of
				[] -> parse q inp
				[(v, out)] -> [(v, out)]


(>>==) :: MyParser a -> (a -> MyParser b) -> MyParser b
p >>== f = \inp -> case parse p inp of
					[] -> []
					[(v, out)] -> parse (f v) out
sat :: (Char -> Bool) -> MyParser Char
--sat p = do
--		x <- item
--		if p x then myReturn x else failure
sat p = item >>== \x ->
		(if p x then myReturn x else failure)

p :: MyParser (Char, Char)
p = item >>== \x ->
	item >>== \z ->
	item >>== \y ->
	myReturn (x, y)

digit :: MyParser Char
digit = sat isDigit	

char :: Char -> MyParser Char
char c = sat (== c)

string :: String -> MyParser String
string [] = myReturn []
string (x : xs) = char x >>== \a ->
				  string xs >>== \b ->
				  myReturn (x : xs)

-- myReturn [] is not []
many :: MyParser a -> MyParser [a]
many p = many1 p +++ myReturn []

many1 :: MyParser a -> MyParser [a]
many1 p = p >>== \x ->
		  many p >>== \xs ->
		  myReturn (x : xs)	

space :: MyParser ()
space = many (sat isSpace) >>== \x ->
		myReturn ()

token :: MyParser a -> MyParser a
token p = space >>== \x ->
		  p >>== \y ->
		  space >>== \z ->
		  myReturn y

symbol :: String -> MyParser String
symbol xs = token (string xs)

--exercise
comment :: MyParser ()
comment = symbol "--" >>== \x ->
		  endOfLine >>== \y ->
		  myReturn ()
--should be /n
endOfLine :: MyParser String
endOfLine = symbol "aa"
			+++ ( item >>== \x ->
				  endOfLine >>== \y ->
				  myReturn y
				)
evalEx :: String -> a
evalEx xs = case parse comment xs of
				[(_, [])] -> error "Correct"
				[(_, out)] -> error "unused input"
				[] -> error "invalid input"

nat :: MyParser Int
nat = many1 digit >>== \xs ->
      myReturn (read xs)

natural :: MyParser Int
natural = token nat

expr :: MyParser Int
expr = term >>== \t ->
        ((
       	symbol "+" >>== \y ->
       	expr >>== \e ->
       	myReturn (t + e)
       	)
        +++ myReturn t
        )

term :: MyParser Int
term = factor >>== \f ->
       ((
       	 symbol "*" >>== \x ->
       	 term >>== \t ->
       	 myReturn (f * t)
       	)
        +++ myReturn f
       	)

factor :: MyParser Int
factor = (symbol "(" >>== \x ->
          expr >>== \e ->
          symbol ")" >>== \y ->
          myReturn e
          ) +++ natural

evalExpr :: String -> Int
evalExpr xs =  case (parse expr xs) of
                    [(n,[])]  -> n
                    [(_,out)] -> error ("unused input " ++ out)
                    []        -> error "invalid input"


