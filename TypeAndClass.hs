import Control.Applicative (Applicative(..))
import Control.Monad       (liftM, ap)

data Nat = Zero | Succ Nat

instance Show Nat where
	show Zero = "Zero"
	show (Succ n) = "Succ" ++ "(" ++ (show n) ++ ")"

myAdd :: Nat -> Nat -> Nat
myAdd Zero n = n
myAdd (Succ m) n = Succ (myAdd m n)

myMult :: Nat -> Nat -> Nat
myMult Zero n = Zero
myMult (Succ Zero) n = n
myMult (Succ m) n = myAdd n (myMult m n)

data Tree = Leaf Int | Node Tree Tree

instance Show Tree where
	show (Leaf n) = "Leaf " ++ show n
	show (Node l r) = "(" ++ "Node " ++ (show l) ++ "  " ++ (show r) ++ ")"

leafNum :: Tree -> Int
leafNum (Leaf _) = 1
leafNum (Node l r) = leafNum l + leafNum r

balanced :: Tree -> Bool
balanced (Leaf _) = True
balanced (Node l r) = abs (leafNum l - leafNum r) <= 1 && balanced l && balanced r

balance :: [Int] -> Tree	
balance [a] = Leaf a
balance xs = Node (balance (take (length xs `div` 2) xs)) (balance (drop (length xs `div` 2) xs))

data Prop = Const Bool | Var Char | Not Prop | And Prop Prop | Imply Prop Prop | Or Prop Prop | Eq Prop Prop

type Assoc k v = [(k, v)]

find :: Eq k => k -> Assoc k v -> v
find k s = head [ v | (kk, v) <- s, k == kk]

type Subst = Assoc Char Bool

eval :: Subst -> Prop -> Bool
eval _ (Const b) = b
eval s (Var c) = find c s
eval s (Not p) = not (eval s p)
eval s (And p q) = (eval s p) && (eval s q)
eval s (Imply p q) = (eval s p) <= (eval s q)
eval s (Or p q) = (eval s p) || (eval s q)
eval s (Eq p q) = (eval s (Imply p q)) && (eval s (Imply q p))

data Expr = Val Int | Mul Expr Expr

value :: Expr -> Int
value (Val n) = n
value (Mul n m) = value n * value m

type Cont = [Op]
data Op = EVAL Expr | MUL Int

eval :: Expr -> Cont -> Int
eval (Val n) c = exec c n
eval (Mul x y) c = eval x (EVAL y : c)

exec :: Cont -> Int -> Int
exec [] n = n
exec (EVAL y : c) n = eval y (MUL n : c)
exec (MUL x : c) n = exec c x * n

data MyMaybe a = MyNothing | MyJust a

instance Functor MyMaybe where
    fmap = liftM

instance Applicative MyMaybe where
    pure  = \a -> MyJust a 
    (<*>) = ap
            
--	return :: a -> MyMaybe a
--	(>>=) :: MyMaybe a -> (a -> MyMaybe b) -> MyMaybe b
instance Monad MyMaybe where
	f >>= g   = case f of
					 MyNothing -> MyNothing
					 MyJust a  -> g a			 					 

-- return :: a -> [a]
-- (>>)   :: [a] -> (a -> [b]) -> [b]

--instance Functor [] where
--	fmap = liftM

--instance Applicative [] where
--	pure = \a -> [a]
--	(<*>) = ap

--instance Monad [] where
--	f >>= g = case f of
--				   []	-> []
--				   [a]	-> g a

