fibs :: [Integer]
fibs = 0 : 1 : [x + y | (x, y) <- zip fibs (tail fibs)]

fib :: Int -> Integer
fib n | n < 0 = error "index error"
      | otherwise = fibs !! n

expr = fib (length (takeWhile (<= 1000) fibs))

data Tree a = Leaf | Node (Tree a) a (Tree a)

instance Show (Tree a) where
    show Leaf = "(Leaf)"
    show (Node l a r) = "(Node" ++ show l ++ "a" ++ show r ++ ")"

repeatTree :: a -> Tree a
repeatTree x = Node l x r
                where l = repeatTree x
                      r = repeatTree x

takeTree :: Int -> Tree a -> Tree a
takeTree 0 _ = Leaf
takeTree n Leaf = Leaf
takeTree n (Node l x r) = Node (takeTree (ceiling (fromIntegral (n - 1) / 2)) l) x (takeTree ((n - 1) `div` 2) r)

replicateTree :: Int -> a -> Tree a
replicateTree n x = takeTree n (repeatTree x)