_replicate n b = [b | _ <- [1..n]]

_pyths n = [(x, y, z) | x <- [1..n], y <- [1..n], z <- [1..n], x /= y, y /= z,  x /= z, x^2 + y^2 == z^2]

perfects n = [x | x <- [1..n], sum ((\y -> [z | z <- [1..y-1], mod y z == 0])x) == x]

find k t = [v | (kk, v) <- t , k == kk ]

_positions x xs = find x (zip xs [0..length xs])

scalarproduct xs ys = sum [x * y | (x, y) <- zip xs ys]

nestedCompresion n m = concat [[ (x, y) | y <- [4..m]] | x <- [1..n]]