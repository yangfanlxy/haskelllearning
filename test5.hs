safetail xs = if null xs then xs else (\(_ : yx) -> yx)xs

_safetail xs | null xs = xs
			 | otherwise =  (\(_ : yx) -> yx)xs

__safetail [] = []
__safetail (_ : ys) = ys

True || True = True
True || False = True
False || True = True
False || False = False

and_ (x, y) = if x == True then (if y == True then True else False)
							else False

and__ (x, y) = if x == True then y else False							
