n = div a (length xs)
	where
		a = 10
		xs = [1, 2, 3, 4, 5]

last_ xs = head (reverse xs)

last__ xs = head (drop (length xs - 1) xs)

init_ xs = take (length xs - 1) xs

init__ xs = reverse (tail (reverse xs))

add x y = x + y

zeroto n = [0..n]