import Data.Char

myAll :: (a -> Bool) -> [a] -> Bool
myAll f = foldr (&&) True . map f

myAny :: (a -> Bool) -> [a] -> Bool 
myAny f = foldr (||) False . map f

myIsLower :: Char -> Bool
myIsLower a | a >= 'a' && a <= 'z' = True
			| otherwise 		   = False

myTakeWhile :: (a -> Bool) -> [a] -> [a]
myTakeWhile p [] = []
myTakeWhile p (x : xs) | p x 	   = x : myTakeWhile p xs
					   | otherwise = []

myDropWhile :: (a -> Bool) -> [a] -> [a]
myDropWhile p [] = []
myDropWhile p (x : xs) | p x 		= myDropWhile p xs 
					   | otherwise	= (x : xs)			   

myMap :: (a -> b) -> [a] -> [b]
myMap f = foldr	(\x y -> (f x) : y) []

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter p = foldr (\x y -> case () of
							  _ | p x       -> x : y
								| otherwise -> y
					) []				   

dec2int :: [Int] -> Int
dec2int xs = foldl (\x (m, n) -> x + m * n) 0 (zip (iterate (*10) 1) (reverse xs))

--5. list elements are same type

myCurry :: ((a, b) -> c) -> (a -> b -> c)
myCurry f = \x -> (\y -> f (x, y))
--test
testF (x, y) = x + y

myUnCurry :: (a -> b -> c) -> ((a, b) -> c)
myUnCurry f = \(x, y) -> f x y
--test
testFF x y = x + y

unfold p h t x | p x 	   = []		 
			   | otherwise = h x : unfold p h t (t x)

chop8 :: [Int] -> [[Int]]
chop8 = unfold null (take 8) (drop 8)

myMap2 :: (a -> b) -> [a] -> [b]
myMap2 f = unfold null (\x -> f (head x)) tail

myIterate :: (a -> a) -> a -> [a]
myIterate f = unfold (\_ -> False) id f

int2bin :: Int -> [Int]
int2bin = unfold (== 0) (`mod` 2) (`div` 2)

make8 :: [Int] -> [Int]
make8 x = take 8 (x ++ repeat 0)

encode :: String -> [Int]
encode = concat . map ((\xs -> xs ++ [(sum xs) `mod` 2])
						. make8 . int2bin . ord)
chop9 :: [a] -> [[a]]
chop9 = unfold null (take 9) (drop 9)

checkCorrect :: [Int] -> Bool
checkCorrect = and . map(\xs -> (last xs) == (sum (init xs)) `mod` 2) . chop9