import Data.Char
import System.IO

main :: IO ()
main = withFile "C:/Users/Lxy/Desktop/sentences.txt" ReadMode $
         \inHandle -> do hSetEncoding inHandle utf8_bom
                         contents <- hGetContents inHandle
                         writeToFile contents

writeToFile :: String -> IO ()
writeToFile xs = withFile "C:/Users/Lxy/Desktop/removeChineseSentences.txt" WriteMode $
                   \outHandle -> do hSetEncoding outHandle utf8_bom
                                    hPutStr outHandle (unlines . map (filter isChineseChar) . lines $ xs) 

isChineseChar :: Char -> Bool
isChineseChar c =  (chr 0x4E00) <= c && c <= (chr 0x9FFF)