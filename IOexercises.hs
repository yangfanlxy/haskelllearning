import Test10

readLine :: IO String
readLine = myRead ""

myRead :: String -> IO String
myRead xs = do x <- getCh
               myProcess x xs

myProcess :: Char -> String -> IO String
myProcess x xs | x == '\n'    = do putChar '\n'
                                   return xs
               | x == '\DEL'  = if null xs
                                   then myRead xs
                                   else do putStr "\ESC[1D"
                                           putChar ' '
                                           putStr "\ESC[1D"
                                           myRead (init xs)
               | otherwise    = do putChar x
                                   myRead (xs ++ [x])