module Test10 where

import System.IO
import Test9

getCh :: IO Char
getCh = do hSetEcho stdin False
           c <- getChar
           hSetEcho stdin True
           return c

myGetChar :: IO Char
myGetChar = do x <- getCh
               putChar x
               return x

myGetLine :: IO String
myGetLine = do x <- getChar
               if x == '\n' then
                    return []
                else
                    do xs <- myGetLine
                       return (x : xs)

myPutStrLn :: String -> IO ()
myPutStrLn xs = do putStr xs
                   putChar '\n'

myStrlen :: IO ()
myStrlen = do putStr "Please input String:"
              xs <- myGetLine
              putStr "The Sting has "
              putStr (show (length xs))
              myPutStrLn " characters"
              return ()

beep :: IO ()
beep = putStr "\BEL"

cls :: IO ()
cls = putStr "\ESC[2J"

type Pos = (Int, Int)

goto :: Pos -> IO ()
goto (x, y) = putStr ("\ESC[" ++ show y ++ ";" ++ show x ++ "H")

writeat :: Pos -> String -> IO ()
writeat p xs = do goto p
                  putStr xs

seqn :: [IO a] -> IO ()
seqn [] = return ()
seqn (x : xs) = do x
                   seqn xs

box :: [String]
box = ["+---------------+",
       "|               |",
       "+---+---+---+---+",
       "| q | c | d | = |",
       "+---+---+---+---+",
       "| 1 | 2 | 3 | + |",
       "+---+---+---+---+",
       "| 4 | 5 | 6 | - |",
       "+---+---+---+---+",
       "| 7 | 8 | 9 | * |",
       "+---+---+---+---+",
       "| 0 | ( | ) | / |",
       "+---+---+---+---+"]

showBox :: IO ()
showBox = seqn [writeat (1, y ) xs | (y, xs) <- zip [1..13] box]

buttons :: [Char]
buttons = standard ++ extra
          where 
            standard = "qcd=123+456-789*0()/"
            extra = "QCD \ESC\BS\DEL\n"

display :: String -> IO()
display xs = do writeat(3, 2) "             "
                writeat(3, 2) (reverse (take 13 (reverse xs)))

calc :: String -> IO()
calc xs = do display xs 
             c <- getCh
             if elem c buttons then
                process c xs
              else
                do beep
                   calc xs

process :: Char -> String -> IO()
process c xs
    |   elem c "qQ\ESC"     = quit
    |   elem c "dD\BS\DEL"  = delete xs
    |   elem c "=\n"        = eval10 xs
    |   elem c "cC"         = clear
    |   otherwise           = press c xs

quit ::IO ()
quit = goto (1, 14)

delete :: String -> IO ()
delete "" = calc ""
delete xs = calc (init xs)

eval10 :: String -> IO ()
eval10 xs = case parse expr xs of
            [(n, "")] -> calc (show n)
            _ -> do beep
                    calc xs

clear :: IO ()
clear = calc ""

press :: Char -> String -> IO ()
press c xs = calc (xs ++ [c])

run :: IO ()
run = do cls
         showBox
         clear


