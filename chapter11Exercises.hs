subs :: [a] -> [[a]]
subs [] = [[]]
subs (x : xs) = subs xs ++ map (x:) (subs xs)

interleave :: a -> [a] -> [[a]]
interleave x [] = [[x]]
interleave y (x : xs) = [y : (x : xs)] ++ map (x:) (interleave y xs)

perm :: [a] -> [[a]]
perm [] = [[]]
perm (x : xs) = concat (map (interleave x) (perm xs))

choices :: [a] -> [[a]]
choices xs = [ zs | ys <- subs xs, zs <- perm ys]

takeSpecficElem :: Eq a => a -> [a] -> [a]
takeSpecficElem n [] = []
takeSpecficElem n (x : xs) | n == x    = xs
                           | otherwise = x : (takeSpecficElem n xs)

isChoice :: Eq a => [a] -> [a] -> Bool
isChoice [] _ = True
isChoice (x : xs) [] = False
isChoice (x : xs) ys = elem x ys && isChoice xs (takeSpecficElem x ys)

--3. No Effect

split :: [a] -> [([a], [a])]
split [] = []
split [_] = []
split (x : xs) = ([x], xs) : [(x : ls, rs) | (ls, rs) <- split xs]

data Op = Add | Sub | Mul | Div

valid :: Op -> Int -> Int -> Bool
valid Add x y = True
valid Sub x y = True
valid Mul x y = True
valid Div x y = y /= 0 && x `mod` y == 0 

apply :: Op -> Int -> Int -> Int
apply Add x y = x + y
apply Sub x y = x - y
apply Mul x y = x * y
apply Div x y = x `div` y

data Expr = Val Int | App Op Expr Expr

type Result = (Expr, Int)

result :: [Int] -> [Result]
result [x] = [(Val x, x)]
result xs = [ res | (l, r) <- split xs,
                   rl <- result l,
                   rr <- result r,
                   res <- combine rr rl]

ops :: [Op]
ops = [Add, Sub, Mul, Div]

combine :: Result -> Result -> [Result]
combine (l, x) (r, y) = [(App o l r, apply o x y)| o <- ops, valid o x y]

solution :: [Int] -> Int -> Int
solution ns n =  length [epr | xs <- choices ns,
                      (epr, res) <- result xs]