halve xs = (take half xs, drop half xs)
				where half = div (length xs) 2