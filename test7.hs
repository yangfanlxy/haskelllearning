-- non-negative integers
(^^^) :: Int -> Int -> Int
0 ^^^ n | n == 0  = 1
	   	| otherwise   = 0
n ^^^ 0 = 1
n ^^^ m = n ^^^ (m - 1) * n

myAdd :: [Bool] -> Bool
myAdd [] = True
myAdd (x : xs) = x && myAdd xs

myConcat :: [[a]] -> [a]
myConcat [] = []
myConcat (x : xs) = x ++ myConcat xs 

myReplicate :: Int -> a -> [a]
myReplicate 0 _ = []
myReplicate n a = a : myReplicate (n - 1) a	

myCC :: [a] -> Int -> a
myCC [] n = error "yangFan empty list"
myCC (x : xs) 0 = x
myCC (x : xs) n = myCC xs (n - 1)

myElem :: Eq a => a -> [a] -> Bool
myElem x [] = False
myElem x (y : ys) = x == y || myElem x ys

myMerge :: Ord a => [a] -> [a] -> [a]
myMerge [] xs = xs
myMerge xs [] = xs
myMerge (x : xs) (y : ys) | x <= y     = x : myMerge xs (y : ys)
						  | otherwise  = y : myMerge (x : xs) ys 

--myHalve :: [a] -> ([a], [a])

myMergeSort :: Ord a => [a] -> [a]
myMergeSort [] = []
myMergeSort [x] = [x]
myMergeSort xs = myMerge first second
				 where  
					first = myMergeSort (take (div (length xs) 2) xs)
					second = myMergeSort (drop (div (length xs) 2) xs)

mySum :: Num a => [a] -> a
mySum [] = 0
mySum (x : xs) = x + mySum xs

--negative number has problem
myTake :: Int -> [a] -> [a]
myTake 0 _ = []
myTake _ [] = []
myTake n (x : xs) | n > 0 	  = x : myTake (n - 1) xs
				  | otherwise = []

myLast :: [a] -> a
myLast [] = error "yangFan, empty list"
myLast [x] = x
myLast (_ : xs) = myLast xs